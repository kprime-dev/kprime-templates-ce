# Design Sprint Methodology

[Overview](https://designsprintkit.withgoogle.com/methodology/overview)

The Design Sprint follows six phases: 
1. Understand, 
2. Define, 
3. Sketch, 
4. Decide, 
5. Prototype, and 
6. Validate. 

[Tools](https://designsprintkit.withgoogle.com/resources/tools)

---
1. Undestrand

In the Understand phase, you will create a shared knowledge base across all participants. Using the Lightning Talk method, knowledge experts across the business are invited to articulate the problem space from business, user, competitor, and technological angles. 


---
2. Define

In the Define phase, the team evaluates everything they learned in the Understand phase to establish focus. This is done by defining specific context and desired outcomes of potential solutions. The phase concludes by choosing a specific focus for your Sprint, as well as goals, success metrics, and signals. 

---
3. Sketch

In the Sketch phase, the Design Sprint team generates and shares a broad range of ideas as individuals. You will start by looking for inspiration, such as solutions in alternative spaces. Then, each Design Sprint participant will individually generate ideas for consideration. From there, the team will narrow down ideas as group to a single, well-articulated Solution Sketch per person. 

---
4. Decide

In the Decide phase, the Design Sprint team finalizes the direction or concept to be prototyped. Each participant will share their Solution Sketch, and the team will find consensus on a single idea through decision-making exercises. The final direction will aim to address the Design Sprint focus. 

---
5. Prototype

In the Prototype phase, the Design Sprint team will work together to create a prototype of your concept. This is when many decisions are made around what exactly the concept is and includes. You will aim to create a prototype that is just real enough to validate, and you will do it really fast!

In the context of Design Sprint, we use the word “prototype” in a slightly different way than in standard product development. A Design Sprint prototype is a facade of the experience you have envisioned in the Sketch phase. You are building just what you need to make the prototype real enough to get an authentic response from a potential user in the Validate phase. This means mapping out the exact flow for the experience and only building the steps you want to test. There is no need to build a full functional back-end or to solve for every flow in your product.

You can think of your prototype as an experiment in order to test out a hypothesis. This means you have to think critically about what you will build in order to get the feedback you need to validate or invalidate your hypothesis. Anything can be prototyped in a day if it is clearly mapped out. 

---
6. Validate

In the Validate phase, the Design Sprint team will put your concept in front of users - this is your moment of truth! You will gather feedback from users who interact with your prototype, and if relevant, you will conduct stakeholder and technical feasibility reviews. You’ll end your Sprint with a validated concept– or an invalidated concept to improve on. Either way, you’ve made progress.

